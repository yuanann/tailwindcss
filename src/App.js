import logo from './logo.svg';
import './App.css'; 
// import './Flex.css';


function App() {
  return (
    <div className="App">
      <div class="flex flex-row">
        <div class="basis-1/4">01</div>
        <div class="basis-1/4">02</div>
        <div class="basis-1/4">03</div>
        <div class="basis-1/4">04</div>
      </div>
            
      <div class="w-full px-4">
        <div class="text-xs">中文</div>
        <div>中文</div>
      </div>

      <header className="App-header">
        <h2 class='flex'>Hi Ryan</h2> 

        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> 
      


    </div>
  );
}

export default App;
